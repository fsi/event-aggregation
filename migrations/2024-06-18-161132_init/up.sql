create table tokens (
    token Text primary key not null,
    username Text not null,
    timestamp BigInt not null
);
