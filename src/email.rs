use lettre::{AsyncSmtpTransport, AsyncTransport, Message, Tokio1Executor};

use async_trait::async_trait;

use lettre::message::{header::ContentType, Attachment, Body, MultiPart, SinglePart};

use anyhow::anyhow;

use std::path::Path;

use crate::{EmailConfig, Messenger};

pub struct Email {
    config: EmailConfig,
}

impl Email {
    pub fn new(config: EmailConfig) -> Email {
        Email { config }
    }
}

#[async_trait]
impl Messenger for Email {
    fn id(&self) -> &str {
        "email"
    }

    fn name(&self) -> &str {
        "E-Mail"
    }

    async fn send(
        &self,
        subject: &str,
        message: &str,
        attachment: Option<&Path>,
    ) -> anyhow::Result<()> {
        let builder = Message::builder()
            .from(self.config.from.parse()?)
            .reply_to(self.config.from.parse()?)
            .to(self.config.to.parse()?)
            .subject(subject);

        let email = match attachment {
            Some(path) => {
                let content_id = String::from("attach-image");
                let filebody = rocket::tokio::fs::read(path).await?;
                let mime =
                    infer::get_from_path(path)?.ok_or(anyhow!("Failed to guess mime type"))?;
                let content_type = ContentType::parse(mime.mime_type())?;
                let attachment = Attachment::new_inline(content_id).body(filebody, content_type);

                builder.multipart(
                    MultiPart::mixed()
                        .singlepart(
                            SinglePart::builder()
                                .header(ContentType::TEXT_PLAIN)
                                .body(Body::new(message.to_string())),
                        )
                        .singlepart(attachment),
                )?
            }
            None => builder
                .header(ContentType::TEXT_PLAIN)
                .body(message.to_string())?,
        };

        let mailer =
            AsyncSmtpTransport::<Tokio1Executor>::from_url(&self.config.smtp_server)?.build();

        // Send the email
        mailer.send(email).await?;
        Ok(())
    }
}
