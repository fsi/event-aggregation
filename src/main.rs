use spline_oauth::{routes::AuthenticatedUser, OAuthConfig};
use async_trait::async_trait;
use email::Email;
use log::warn;
use maud::{html, Markup, DOCTYPE};
use rocket::{
    form::{Form, FromForm},
    fs::{FileServer, TempFile},
    get, post,
    response::Redirect,
    routes, uri, State,
};
use serde::Deserialize;
use std::{collections::HashMap, path::Path};

mod email;
mod mastodon;
mod telegram;

use mastodon::Mastodon;
use telegram::Telegram;

fn inherit_base_template(inner: Markup) -> Markup {
    html! {
        (DOCTYPE)
        html {
            head {
                link href="/static/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous";
                meta name="viewport" content="width=device-width, initial-scale=1";
                title { "FSI Cross Post" }
            }
            body class="p-3 m-0 border-0" {
                div class="container" style="max-width: 40rem; margin: 0 auto;" {
                    div class="card p-4 container justify-content-center" {
                        (inner)
                    }
                }
                script src="/static/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous" {}
            }
        }
    }
}

#[derive(FromForm)]
struct PostForm<'r> {
    subject: &'r str,
    content: &'r str,
    attachment: Option<TempFile<'r>>,
    messengers: HashMap<String, bool>,
}

#[post("/post", data = "<form>")]
async fn post_message(
    form: Form<PostForm<'_>>,
    state: &State<Vec<Box<dyn Messenger + Send + Sync + 'static>>>,
    _user: AuthenticatedUser,
) -> Markup {
    for messenger in state.inner() {
        let enabled = match form.messengers.get(messenger.id()) {
            Some(enabled) => *enabled,
            None => false,
        };

        if !enabled {
            warn!("Skipping {}", messenger.name());
            continue;
        }

        warn!("Sending to {}", messenger.name());

        match messenger
            .send(
                form.subject,
                form.content,
                form.attachment
                    .as_ref()
                    .and_then(|a| if a.len() > 0 { Some(a) } else { None })
                    .and_then(|a| a.path()),
            )
            .await
        {
            Ok(()) => {}
            Err(e) => {
                return inherit_base_template(html! {
                    h1 { "Error :(" }

                    p { "The API returned " (e) }
                })
            }
        }
    }

    inherit_base_template(html! {
        h1 { "Success" }
    })
}

#[get("/post")]
async fn write_post(
    messengers: &State<Vec<Box<dyn Messenger + Send + Sync + 'static>>>,
    _user: AuthenticatedUser,
) -> Markup {
    inherit_base_template(html! {
        h1 { "Post to all channels" }


        form class="mt-2" id="post-form" method="post" action="/post" enctype="multipart/form-data" {
            @for messenger in messengers.inner() {
                @let id = format!("messengers.{}", messenger.id());

                div class="form-check" {
                    input type="checkbox" id=(messenger.id()) name=(id) class="form-check-input";
                    label for=(messenger.id()) class="form-check-label" { (messenger.name()) }
                }
            }

            input name="subject" type="text" class="form-control mt-4" placeholder="Subject";

            textarea name="content" form="post-form" class="form-control mt-2" placeholder="Content" { }

            input type="file" name="attachment" class="form-control mt-2";

            input type="submit" class="btn btn-primary mt-4" value="Post";
        }

    })
}

#[get("/")]
async fn index() -> Redirect {
    let target = uri!(write_post).to_string();
    Redirect::to(uri!(spline_oauth::routes::login(target)))
}

#[async_trait]
trait Messenger {
    fn name(&self) -> &str;
    fn id(&self) -> &str;
    async fn send(
        &self,
        subject: &str,
        message: &str,
        attachment: Option<&Path>,
    ) -> anyhow::Result<()>;
}

#[derive(Deserialize)]
struct TelegramOptions {
    token: String,
    chat_id: i64,
}

#[derive(Deserialize)]
struct MastodonOptions {
    instance_url: String,
    auth: Option<mastodon_async::data::Data>,
}

#[derive(Deserialize)]
struct EmailConfig {
    smtp_server: String,
    from: String,
    to: String,
}

#[derive(Default, Deserialize)]
struct Config {
    telegram: Option<TelegramOptions>,
    mastodon: Option<MastodonOptions>,
    email: Option<EmailConfig>,
}

#[rocket::launch]
async fn rocket() -> _ {
    let rocket = rocket::build();

    let figment = rocket.figment();
    let config: Config = figment.extract().expect("Failed to read config");
    let oauth_config: OAuthConfig = figment
        .extract_inner("oauth")
        .expect("Reading oauth config");

    let mut messengers = Vec::<Box<dyn Messenger + Send + Sync>>::new();

    if let Some(telegram) = config.telegram {
        messengers.push(Box::new(
            Telegram::new(telegram.token, telegram.chat_id)
                .await
                .unwrap(),
        ))
    }

    if let Some(mastodon) = config.mastodon {
        messengers.push(Box::new(
            Mastodon::new(&mastodon.instance_url, mastodon.auth)
                .await
                .unwrap(),
        ))
    }

    if let Some(email) = config.email {
        messengers.push(Box::new(Email::new(email)))
    }

    rocket
        .mount("/", routes![index, write_post, post_message])
        .manage(messengers)
        .mount("/static", FileServer::from("static"))
        .attach(spline_oauth::OAuthSupport::fairing(oauth_config))
}
