use crate::Messenger;
use async_trait::async_trait;
use mastodon_async::status_builder::StatusBuilder;
use mastodon_async::{helpers::cli, prelude::*, Mastodon as MastodonApi};
use std::{borrow::Cow, path::Path};

pub struct Mastodon {
    api: MastodonApi,
}

impl Mastodon {
    pub async fn new(
        instance_url: &str,
        mastodon_auth: Option<mastodon_async::data::Data>,
    ) -> anyhow::Result<Mastodon> {
        match mastodon_auth {
            Some(mut auth) => {
                auth.base = Cow::from(instance_url.to_string());
                Ok(Mastodon {
                    api: MastodonApi::from(auth),
                })
            }
            None => {
                let registration = Registration::new(instance_url)
                    .client_name("multiannounce")
                    .scopes(
                        Scopes::write(mastodon_async::scopes::Write::Statuses)
                            | Scopes::write(mastodon_async::scopes::Write::Media),
                    )
                    .build()
                    .await?;

                let api = cli::authenticate(registration).await?;

                eprintln!(
                    "Add the following to the mastodon.auth config section to store the log in data: \n\n{}",
                    toml::to_string(&api.data).unwrap()
                );
                Ok(Mastodon { api })
            }
        }
    }
}

#[async_trait]
impl Messenger for Mastodon {
    fn id(&self) -> &str {
        "mastodon"
    }

    fn name(&self) -> &str {
        "Mastodon"
    }

    async fn send(
        &self,
        subject: &str,
        message: &str,
        attachment: Option<&Path>,
    ) -> anyhow::Result<()> {
        let text = format!("{subject}\n\n{message}");

        if let Some(path) = attachment {
            let media = self.api.media(path, None).await?;
            self.api
                .new_status(
                    StatusBuilder::new()
                        .status(text)
                        .media_ids(&[media.id])
                        .build()?,
                )
                .await?;
        } else {
            self.api
                .new_status(StatusBuilder::new().status(text).build()?)
                .await?;
        };

        Ok(())
    }
}
