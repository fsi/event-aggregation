use crate::Messenger;

use anyhow::anyhow;
use async_trait::async_trait;
use std::path::Path;
use telbot_ureq::types::message::SendPhoto;
use telbot_ureq::types::{file::InputFile, markup::ParseMode, message::SendMessage};
use telbot_ureq::Api;

use std::string::ToString;

pub struct Telegram {
    api: Api,
    chat_id: i64,
}

impl Telegram {
    pub async fn new(token: String, chat_id: i64) -> anyhow::Result<Telegram> {
        Ok(Telegram {
            api: Api::new(token),
            chat_id,
        })
    }
}

#[async_trait]
impl Messenger for Telegram {
    fn id(&self) -> &str {
        "telegram"
    }

    fn name(&self) -> &str {
        "Telegram"
    }

    async fn send(
        &self,
        subject: &str,
        message: &str,
        attachment: Option<&Path>,
    ) -> anyhow::Result<()> {
        let text = format!("*{subject}*\n\n{message}");

        match attachment {
            Some(path) => {
                let data = rocket::tokio::fs::read(path).await?;

                let mime = infer::get(&data).ok_or(anyhow!("Failed to guess mime type"))?;

                let photo = InputFile {
                    name: "photo".to_string(),
                    data,
                    mime: mime.to_string(),
                };

                let mut send_photo = SendPhoto::new(self.chat_id, photo);
                send_photo.caption = Some(text);
                send_photo.parse_mode = Some(ParseMode::MarkdownV2);
                self.api
                    .send_file(&send_photo)
                    .map_err(|e| anyhow!("{e:?}"))?;
            }
            None => {
                let mut msg = SendMessage::new(self.chat_id, text);
                msg.parse_mode = Some(ParseMode::MarkdownV2);
                self.api.send_json(&msg).map_err(|e| anyhow!("{e:?}"))?;
            }
        }
        Ok(())
    }
}
